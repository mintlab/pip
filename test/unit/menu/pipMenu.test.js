/*global angular,describe, beforeEach, module, it, expect,inject*/
describe('pipMenu', function() {
	
	var element,
		$rootScope,
		$compile,
		ctrl,
		menuItem,
		bg,
		menuService;
	
	beforeEach(module('templates'));	
	beforeEach(module('PIP'));
	
	beforeEach(inject([ '$rootScope', '$compile', 'menuService', function ( ) {
		
		$rootScope = arguments[0];
		$compile = arguments[1];
		menuService = arguments[2];
		
		element = angular.element('<pip-menu></pip-menu>');
		$compile(element)($rootScope);
		$rootScope.$digest();
		
		ctrl = element.controller('pipMenu');
		
		menuItem = element.find('ul').find('a').eq(0)[0];
		bg = element[0].querySelector('.pip-menu-bg');
		
	}]));
		
	it('should have a controller', function ( ) {
		expect(!!ctrl).toBe(true);
	});
	
	it('should be the same as menuService.isOpen', function ( ) {
		expect(ctrl.isOpen()).toBe(menuService.isOpen());
	});
	
	it('should be closed', function ( ) {
		menuService.close();
		expect(ctrl.isOpen()).toBe(false);
	});
	
	it('should be open', function ( ) {
		menuService.open();
		expect(ctrl.isOpen()).toBe(true);
	});
	
	it('should close after an item click', function ( ) {
		
		expect(!!menuItem).toBe(true);
		
		menuService.open();
		
		expect(ctrl.isOpen()).toBe(true);
		
		angular.element(menuItem).triggerHandler('click');
		
		expect(ctrl.isOpen()).toBe(false);
		
	});
	
	it('should close after a background click', function ( ) {
		
		expect(!!bg).toBe(true);
		
		menuService.open();
		
		expect(ctrl.isOpen()).toBe(true);
		
		angular.element(bg).triggerHandler('click');
		
		expect(ctrl.isOpen()).toBe(false);
		
		
	});
  
});
