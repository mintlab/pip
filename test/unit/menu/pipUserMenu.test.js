/*global angular,describe, beforeEach, module, it, expect,inject*/
describe('pipUserMenu', function() {
	
	var element,
		$rootScope,
		$compile,
		ctrl;
	
	beforeEach(module('templates'));	
	beforeEach(module('PIP'));
	
	beforeEach(inject([ '$rootScope', '$compile', function ( ) {
		
		$rootScope = arguments[0];
		$compile = arguments[1];
		
		element = angular.element('<pip-user-menu></pip-user-menu>');
		$compile(element)($rootScope);
		$rootScope.$digest();
		
		ctrl = element.controller('pipUserMenu');
		
	}]));
		
	it('should have a controller', function ( ) {
		expect(!!ctrl).toBe(true);
	});
	
	it('should toggle', function ( ) {
		
		var open = ctrl.isOpen();
		
		ctrl.toggle();
		
		expect(ctrl.isOpen()).toBe(!open);
		
	});
  
});
