/*global angular,describe, beforeEach, module, it, expect,inject*/
describe('pipApp', function() {
	
	var element,
		scope;
		
	beforeEach(module('templates'));
	beforeEach(module('PIP'));
	
	beforeEach(inject(function ( $rootScope, $compile ) {
		
		scope = $rootScope;
		element = angular.element('<div pip-app></div>');
		$compile(element)(scope);
		scope.$digest();
		
	}));
	
	describe('when initialized', function ( ) {
		it('should have a controller', function ( ) {
			expect(!!element.controller('pipApp')).toBe(true);
		});
	});
  
});
