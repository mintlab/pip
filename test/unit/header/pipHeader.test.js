/*global angular,describe, beforeEach, module, it, expect,inject*/
describe('pipHeader', function() {
	
	var element,
		$rootScope,
		$compile,
		ctrl,
		menuService;
	
	beforeEach(module('templates'));	
	beforeEach(module('PIP'));
	
	beforeEach(inject([ '$rootScope', '$compile', 'menuService', function ( ) {
		
		$rootScope = arguments[0];
		$compile = arguments[1];
		menuService = arguments[2];
		
		element = angular.element('<pip-header></pip-header>');
		$compile(element)($rootScope);
		$rootScope.$digest();
		
		ctrl = element.controller('pipHeader');
		
	}]));
	
	describe('when used', function ( ) {
		
		it('should have a controller', function ( ) {
			expect(!!ctrl).toBe(true);
		});
		
		it('should be the same as menuService.isOpen', function ( ) {
			expect(ctrl.isMenuOpen()).toBe(menuService.isOpen());
		});
		
		it('should be closed', function ( ) {
			menuService.close();
			expect(ctrl.isMenuOpen()).toBe(false);
		});
		
		it('should be open', function ( ) {
			menuService.open();
			expect(ctrl.isMenuOpen()).toBe(true);
		});
		
		it('should toggle', function ( ) {
			menuService.open();
			
			ctrl.toggleMenu();
			expect(ctrl.isMenuOpen()).toBe(false);
			
			ctrl.toggleMenu();
			expect(ctrl.isMenuOpen()).toBe(true);
		});
		
		
		
	});
  
});
