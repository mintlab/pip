/*global angular*/
(function ( ) {
	
	angular.module('PIP.header')
		.directive('pipHeader', [ 'menuService', function ( menuService ) {
			
			return {
				restrict: 'E',
				scope: true,
				templateUrl: '/html/pip/header/index.html',
				controller: [ function ( ) {
					
					var ctrl = {};
					
					ctrl.isMenuOpen = function ( ) {
						return menuService.isOpen();
					};
					
					ctrl.toggleMenu = function ( ) {
						if(menuService.isOpen()) {
							menuService.close();
						} else {
							menuService.open();
						}
					};
					
					return ctrl;
					
				}],
				controllerAs: 'pipHeader'
			};
			
		}]);
	
})();
