/*global angular*/
(function ( ) {
	
	angular.module('PIP.header')
		.directive('pipSearchBox', [ function ( ) {
			
			return {
				scope: true,
				restrict: 'E',
				templateUrl: '/html/pip/header/search-box.html',
				controller: [ function ( ) {
					
					var ctrl = {},
						open;
											
					function submit ( ) {
						// loading = true;
					}
					
					ctrl.handleSubmit = function ( ) {
						submit();
					};
					
					ctrl.handleFocus = function ( ) {
						open = true;
					};
					
					ctrl.handleBlur = function ( ) {
						open = false;
					};
					
					ctrl.handleButtonClick = function ( ) {
						if(!open) {
							open = true;
						} else {
							submit();
						}
					};
					
					ctrl.open = function ( ) {
						open = true;	
					};
					
					ctrl.close = function ( ) {
						open = false;
					};
					
					ctrl.isOpen = function ( ) {
						return open;	
					};
					
					return ctrl;
					
				}],
				controllerAs: 'pipSearchBox'
			};
			
		}]);
	
})();
