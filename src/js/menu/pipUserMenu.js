/*global angular*/
(function ( ) {
	
	angular.module('PIP.menu')
		.directive('pipUserMenu', [ function ( ) {
			
			return {
				scope: true,
				restrict: 'E',
				templateUrl: '/html/pip/menu/user-menu.html',
				controller: [ function ( ) {

					var ctrl = {},
						open = false;
					
					ctrl.isOpen = function ( ) {
						return open;	
					};
					
					ctrl.toggle = function ( ) {
						open = !open;	
					};
					
					return ctrl;
					
				}],
				controllerAs: 'pipUserMenu'
			};
			
		}]);
	
})();
