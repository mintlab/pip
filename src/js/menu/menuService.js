/*global angular*/
(function ( ) {
	
	angular.module('PIP.menu')
		.factory('menuService', [ function ( ) {
			
			var menuService = {},
				open = false;
				
			menuService.isOpen = function ( ) {
				return open;
			};
			
			menuService.open = function ( ) {
				open = true;
			};
			
			menuService.close = function ( ) {
				open = false;
			};
			
			return menuService;
			
		}]);
	
})();
