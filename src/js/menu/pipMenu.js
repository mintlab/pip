/*global angular*/
(function ( ) {
	
	angular.module('PIP.menu')
		.directive('pipMenu', [ 'menuService', function ( menuService ) {
			
			return {
				restrict: 'E',
				scope: true,
				templateUrl: '/html/pip/menu/index.html',
				controller: [ function ( ) {
					
					var ctrl = {},
						menuItems = [
							{
								label: 'Dashboard',
								url: '/pip',
								num_tasks: 0
							},
							{
								label: 'Zaken',
								url: '/pip/zaken',
								num_tasks: 0
							},
							{
								label: 'WOZ',
								url: '/pip/woz',
								num_tasks: 0
							},
							{
								label: 'Uitkeringen',
								url: '/pip/uitkeringen',
								num_tasks: 0
							}
						];
					
					ctrl.getMenuItems = function ( ) {
						return menuItems;	
					};
					
					ctrl.isOpen = function ( ) {
						return menuService.isOpen();
					};
					
					ctrl.handleItemClick = function ( ) {
						menuService.close();	
					};
					
					ctrl.handleBackgroundClick = function ( ) {
						menuService.close();	
					};
					
					return ctrl;
					
				}],
				controllerAs: 'pipMenu'
			};
			
		}]);
	
})();
