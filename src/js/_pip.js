/*global angular*/
(function ( ) {
	
	angular.module('PIP', [
		'ngRoute',
		'ngAnimate',
		'PIP.menu',
		'PIP.header',
		'PIP.dashboard',
		'PIP.case',
		'pip.config'
	])
		.config([ '$routeProvider', '$locationProvider', function ( $routeProvider, $locationProvider ) {
			
			$locationProvider.html5Mode(true);
			
			$routeProvider.when('/pip/', {
					templateUrl: '/html/pip/dashboard/view.html',
					controller: 'DashboardViewController'
				})
				.when('/pip/zaken/', {
					templateUrl: '/html/pip/case/list-view.html',
					controller: 'CaseListViewController'
				})
				.when('/pip/zaken/:zaakId', {
					templateUrl: '/html/pip/case/single-view.html',
					controller: 'CaseSingleViewController'
				})
				.when('/pip/woz/', {
					templateUrl: '/html/pip/woz/view.html',
					controller: 'WozViewController'
				})
				.when('/pip/uitkeringen/', {
					templateUrl: '/html/pip/benefit/view.html',
					controller: 'BenefitViewController'
				})
				.otherwise({
					templateUrl: '/html/pip/404.html',
					controller: '404ViewController'
				});
				
			
		}]);
	
})();
